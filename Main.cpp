#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Animal voice!" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "What does the dog say& - Woof!" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "What does the cat say? - Meow!" << std::endl;
	}
};

class Bird :public Animal
{
public:
	void Voice() override
	{
		std::cout << "What does the bird say? - Tweet!" << std::endl;
	}
};

int main()
{
	Animal* animal[3];
	animal[0] = new Dog();
	animal[1] = new Cat();
	animal[2] = new Bird();
	
	for (int i = 0; i < 3; i++)
	{
		animal[i]->Voice();
	}
	for (int i = 0; i < 3; i++)
	{
		delete animal[i];
	}
}